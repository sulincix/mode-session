# Mode session

Full featured gnome-session alternative. 

## Features

* Written pure bash and python3
* gnome-sesion-fail removed.
* lower ram usage.
* gnome-shell minimal session

