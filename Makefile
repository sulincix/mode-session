DESTDIR=/
build:
	: please run make install

install: create-dirs install-common install-xorg install-wayland

create-dirs:
	mkdir -p $(DESTDIR)/usr/bin/ || true
	mkdir -p $(DESTDIR)/usr/share/gnome-session/sessions/ || true
	mkdir -p $(DESTDIR)/usr/share/polkit-1/actions/

install-common:
	install src/gnome-session $(DESTDIR)/usr/bin/gnome-session
	install src/gnome-session-quit $(DESTDIR)/usr/bin/gnome-session-quit
	install src/gnome-session-inhibit $(DESTDIR)/usr/bin/gnome-session-inhibit
	ln -s gnome-session $(DESTDIR)/usr/bin/gnome-session-classic || true
	ln -s gnome-session $(DESTDIR)/usr/bin/gnome-session-custom-session || true
	install src/mode-session $(DESTDIR)/usr/bin/mode-session
	install data/gnome.session $(DESTDIR)/usr/share/gnome-session/sessions/
	install data/gnome-dummy.session $(DESTDIR)/usr/share/gnome-session/sessions/
	install dbus-service/gnome-session-manager $(DESTDIR)/usr/bin/
	install data/gnome-session.policy $(DESTDIR)/usr/share/polkit-1/actions/

install-xorg:
	mkdir -p $(DESTDIR)/usr/share/xsessions/ || true
	install data/gnome.desktop $(DESTDIR)/usr/share/xsessions/

install-wayland:
	mkdir -p $(DESTDIR)/usr/share/wayland-sessions/ || true
	install data/gnome-wayland.desktop $(DESTDIR)/usr/share/wayland-sessions/

install-shell-xorg:
	mkdir -p $(DESTDIR)/usr/share/xsessions/ || true
	install data/gnome-shell.desktop $(DESTDIR)/usr/share/xsessions/
install-shell-wayland:
	mkdir -p $(DESTDIR)/usr/share/wayland-sessions/ || true
	install data/gnome-shell-wayland.desktop $(DESTDIR)/usr/share/wayland-sessions/
